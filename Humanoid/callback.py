from stable_baselines3.common.callbacks import BaseCallback
import matplotlib.pyplot as plt
import numpy as np

class PlotLearningCurveCallback(BaseCallback):
    """
    A custom callback that derives from ``BaseCallback`` and plots the cumulative reward per episode.

    :param verbose: Verbosity level: 0 for no output, 1 for info messages, 2 for debug messages
    """
    def __init__(self, save_path: str, verbose: int = 0):
        super().__init__(verbose)
        # Initialize the list to store rewards
        # Those variables will be accessible in the callback
        # (they are defined in the base class)
        # The RL model
        # self.model = None  # type: BaseAlgorithm
        # An alias for self.model.get_env(), the environment used for training
        # self.training_env # type: VecEnv
        # Number of time the callback was called
        # self.n_calls = 0  # type: int
        # num_timesteps = n_envs * n times env.step() was called
        # self.num_timesteps = 0  # type: int
        # local and global variables
        # self.locals = {}  # type: Dict[str, Any]
        # self.globals = {}  # type: Dict[str, Any]
        # The logger object, used to report things in the terminal
        # self.logger # type: stable_baselines3.common.logger.Logger
        # Sometimes, for event callback, it is useful
        # to have access to the parent object
        # self.parent = None  # type: Optional[BaseCallback]
        self.cumulative_rewards = []
        self.episode_rewards = 0
        self.episodes_completed = 0
        self.save_path = save_path

    def _on_training_start(self) -> None:
        """
        This method is called before the first rollout starts.
        """
        pass

    def _on_rollout_start(self) -> None:
        """
        A rollout is the collection of environment interaction
        using the current policy.
        This event is triggered before collecting new samples.
        """
        pass

    def _on_step(self) -> bool:
        """
        This method will be called by the model after each call to `env.step()`.

        For child callback (of an `EventCallback`), this will be called
        when the event is triggered.

        Here, we will collect the rewards and update the cumulative reward per episode.

        :return: If the callback returns False, training is aborted early.
        """
        self.episode_rewards += self.locals['rewards']
        if self.locals['dones']:
            self.cumulative_rewards.append(self.episode_rewards)
            self.episode_rewards = 0
            self.episodes_completed += 1
        
        return True

    def _on_rollout_end(self) -> None:
        """
        This event is triggered before updating the policy.
        """
        pass

    def _on_training_end(self) -> None:
        """
        This event is triggered before exiting the `learn()` method.
        Here, we will plot the cumulative reward per episode.
        """
        plt.figure(figsize=(10, 6))
        mean_rewards = [np.mean(self.cumulative_rewards[max(0, i-10):i]) for i in range(1, self.episodes_completed + 1)]
        plt.plot(range(1, self.episodes_completed + 1), mean_rewards)
        plt.xlabel('Episode')
        plt.ylabel('Mean Cumulative Reward (Last 10 Episodes)')
        plt.title('Mean Cumulative Reward per Episode (Last 10 Episodes)')
        plt.savefig(self.save_path + '/mean_learning_curve.png')
        plt.show()
