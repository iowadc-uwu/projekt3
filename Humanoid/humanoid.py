import gymnasium as gym
import numpy as np
from collections import defaultdict
import tqdm
from stable_baselines3 import SAC, DDPG
from stable_baselines3.common.callbacks import CheckpointCallback, CallbackList

from projekt3.Humanoid.callback import PlotLearningCurveCallback

def make_env(expected_timesteps):
    env = gym.make("Humanoid-v4")
    env = gym.wrappers.RecordEpisodeStatistics(env, deque_size=expected_timesteps*10)
    return env


def train_model(timesteps, model_name: str, discount_factor: float):
    checkpoint_callback = CheckpointCallback(
    save_freq=min(5000, timesteps),
    save_path="./projekt3/Humanoid/logs/" + model_name,
    name_prefix=model_name,
    save_replay_buffer=True,
    save_vecnormalize=True,
    )

    callback = CallbackList([checkpoint_callback, PlotLearningCurveCallback("./projekt3/Humanoid/logs/" + model_name)])

    env = gym.make("Humanoid-v4")
    env = gym.wrappers.RecordEpisodeStatistics(env, deque_size=timesteps*500)

    # model = SAC('MlpPolicy', env, verbose=1, gamma=discount_factor)
    model = DDPG('MlpPolicy', env, verbose=1, gamma=discount_factor)
    model.learn(total_timesteps=timesteps, callback=callback)

def continue_training(timesteps, model, model_name: str):
    checkpoint_callback = CheckpointCallback(
    save_freq=min(1000, timesteps),
    save_path="./projekt3/Humanoid/logs/" + model_name,
    name_prefix=model_name,
    save_replay_buffer=True,
    save_vecnormalize=True,
    )

    callback = CallbackList([checkpoint_callback, PlotLearningCurveCallback("./projekt3/Humanoid/logs/" + model_name)])

    model.learn(total_timesteps=timesteps, callback=callback)

def test_model(n_episodes, model):
    env = gym.make("Humanoid-v4", render_mode="human")

    for episode in tqdm.tqdm(range(n_episodes)):
        cum_reward = 0
        obs, info = env.reset()
        done = False

        while not done:
            action, _states = model.predict(obs, deterministic=True)
            obs, reward, terminated, truncated, info = env.step(action)
            done = terminated or truncated
            cum_reward += reward

        print(f"Cumulative reward: {cum_reward}")
    env.close()

def load_model(path, env):
    model = SAC.load(path, env)
    return model


timesteps = 10000

train_model(timesteps, "ddpg_gamma_0_9", 0.9)

# model1 = load_model("./projekt3/Humanoid/logs/rl_model_20000_steps.zip", make_env(timesteps))
# continue_learning(timesteps, model1, "gamma_0_99_continued")
